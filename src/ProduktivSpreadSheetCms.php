<?php 
namespace ProduktivSpreadSheetCms;

class ProduktivSpreadSheetCms {
    public $options;

    function __construct($options) {
        $this->options = $options;
    }
    
    function fetchGoogleSpreadSheetData() {            
        $googleSpreadSheetURL = "https://spreadsheets.google.com/feeds/cells/{$this->options['spreadsheet_url']}/2/public/full?alt=json";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $googleSpreadSheetURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;            
    }

    public function getSpreadSheetData() { 
        return $this->fetchGoogleSpreadSheetData();        
    }
}

?>